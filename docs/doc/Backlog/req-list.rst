Lists of Requirements
=====================

Open Requirements
-----------------
.. needtable::
   :types: req
   :style: table
   :columns: title;id;status;outgoing;incoming
   :status: import;open;

.. note:: ‘import’ counts as ‘open’

In progress
-----------
.. needtable::
   :types: req
   :style: table
   :columns: title;id;status;outgoing;incoming
   :status: in progress;

New Idea’s
-----------
.. needtable::
   :types: req
   :style: table
   :columns: title;id;status;outgoing;incoming
   :status: idea


