.. (C) ALbert Mietus, Sogeti, 2019

==================
 RoundTrip & Flood
==================

**RoundTrip** and **Flood** are two basic *Hello World* style Demonstrators.
|BR|
A simple message is send around toward several (network) nodes. Typically, each *node* runs on a dedicated Raspberry-Pi. As DDS is network-agnostic; multiple nodes can run on the same computer. By example, on a PC/Laptop, which is great for demonstration and an easy start.

A standard setup is shown below, where 4 nodes are used; a trivial variations is to have more or less nodes
|BR|
One node is called “master”, as it starts the roundtrip; and executes the measurements. The other nodes are called “hub” and have the same functionality; the “links” are set by configuration.

.. uml::

   title “Default setup with 4 nodes”
   !include ../../_inc/class-as-Node.iuml


   class Master << (N, Aqua)>> #Aqua/CornflowerBlue
   class Hub_1  << (N, Aqua)>>
   class Hub_2  << (N, Aqua)>>
   class Hub_3  << (N, Aqua)>>

   Master -right-|> Hub_1  : Topic_0
   Hub_1  -down-|>  Hub_2  : Topic_1
   Hub_2  -left-|>  Hub_3  : Topic_2
   Hub_3  -up-|>    Master : Topic_3

   note as N1
      Here the <i>functional</i> message-flow is shown, not actual flow!
      As “DDS-messages” are *not* directed to a node.
   end note

   note as N2
      This can be implemented by giving each links a (topic)Name; and
      subscribe to the “incoming” name and publish on the “outgoing” name.

      Those topic/names are typically specified at start-up, as command-line options.
   end note

   N1 .. N2

The difference between ‘RoundTrip’ and ‘Flood’ is how *fast* and in which *order* the message are processed.

.. demo:: RoundTrip
   :ID: RT_001
   :tags: RT
   :status: import

   In RoundTrip only **one** message is send around at the same time. The ‘Master’ starts a the sequence by sending one
   message *(by example: “0”)*. And the receiving hub forwards it; often trivially changing it *(in the example: by adding
   “1” to the incoming message)*.

   Only when the ‘master’ receives the message *(let’s say “3”)*, it send a new message. In the example, that would be
   “4” (3+1). And the sequence starts again.

   After an configured number of roundtrips, the master stops sending/forwarding; and the demo is over.

.. demo:: Flood
   :ID: F_001
   :tags: F
   :status: import

   In Flood the ‘hub’ works exactly as in :need:`RT_001`: it receives a message and forwards it. Typically, the message
   is changed by adding a hub-specific postfix *(by example the hub-number)*

   The ‘master’ differs however: it does **NOT** wait for an incoming message to send the next message. Instead it sends
   all message as fast as possible.

   Still, the ‘Master’ has to read the incoming message; but can dispose them; reading **should** have a higher
   priority the sending!


.. toctree::
   :glob:
   :maxdepth: 0

   *
