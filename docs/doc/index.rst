.. (C) ALbert Mietus, Sogeti, 2019

***************************
TechPush: DDS-Demonstrators
***************************

.. sidebar:: Work just started

   * All sources, including for documentation can be found in the official *main repository*:
     https://bitbucket.org/HighTech-nl/dds-demonstrators

   * All development is done in *Feature forks*, using the `Forking Workflow
     <https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow>`_

   * **ONLY** the *main repository* is used to generate the documentation:
     https://DDS-Demonstrators.readthedocs.io/

DDS [#DDS]_ (Data Distribution Service) is a real-time, embedded *“data-centric"* **pub-sub** communication framework. Which is
becoming popular in the (professional, industrial) IoT-world. It is also very *HighTech*; one of the reasons why we
(Sogeti HighTech BL) are interested.

Sogeti-HighTech has bundled several innovation-projects into this **DDS-Demonstrators** program.
|BR|
Each project will result in a simple *Demonstrator*, showing the power of DDS; and *how* to *use* DDS; including
(demonstration) code, test, and lessons-learned [#PR1]_.

Most software is made in C, C++ or Python and will be run an embedded Linux system. Often we use a Raspberry Pi, which is
affordable; and a good reference for a modern embedded system.

* Python is mainly used as a Proof-of-Concept language; when the *execution* speed is less relevant than the
  development-velocity. Python is also  used for scripting (for tests and measurements).
* When speed- or latency-measurements are needed, C/C++ is typically used; possibly after a Python PoC.
* Python is also often used for tool-like demonstrators (such as the :ref:`XScope`)

Main Table of Content
=====================

.. toctree::
   :maxdepth: 2
   :glob:

   DDS
   MeasureBlog
   Backlog/index
   Demonstrators/index
   Teams/index
   todoList

.. rubric:: Footnotes

.. [#DDS]  See: https://en.wikipedia.org/wiki/Data_Distribution_Service
.. [#PR1]  To share this knowledge many *DDS-Demonstrators* are treated as open-source. Then it becomes easy to share it
           with our (over 100) HighTech Software Developers, other Sogeti-professionals, and to show it to clients.
